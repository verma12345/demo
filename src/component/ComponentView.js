import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';

export default class ComponentView extends Component {
  render() {
    return (
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.layout}>{this.props.item.id}</Text>
        {/* {this.props.item.isShown ? (
          <Text style={styles.layout}>{this.props.item.name}</Text>
        ) : null} */}
         <Text style={[styles.layout,{fontWeight:"bold"}]}>{this.props.item.name}</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  layout: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});
