import React, { Component } from "react";
import { View, Text } from "react-native";

export default class Variable extends Component{
    
    render(){
        var x = 53;
        var x = 100;

        let l = 50;
        // let l = 100;
        l = 100;

        const c = 20;
        // const c = 30;
        // c = 30;

        //Spread Operator
        const bank = 'PNB';
        const intrest = '8%'

        var arr = [11,22,33];
        var arr1 = [44,55,66];
        var a = [...arr, ...arr1];

        function test(){
            return {bank ,intrest}
        }

        return(
            <View>
                <Text>{"\tvar:\t>>>\t can be update and redeclare\n"+x+" "+x+"\n\n"
                +"\tlet:\t>>> \tcan be update but can't be redeclare\n"+l+" "+l+"\n\n"
                +"\tconst:\t>>> \tnither update nor redeclare\n"+c+" "+c+"\n\n"+
                "Spread Operator:\t>>>\t is used to copy list of array object.\n"+a+"\n\n"+
                test
                }</Text>
            </View>
        )
    }
}