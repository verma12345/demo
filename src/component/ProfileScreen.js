import React, { Component } from "react";
import { View, Image, Text, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class ProfileScreen extends Component{
    render(){
        return(
           <View style = {styles.container}>
               
               <View>
                   <Image
                   source={{
                    uri:
                    'https://images.vexels.com/media/users/3/147101/isolated/preview/b4a49d4b864c74bb73de63f080ad7930-instagram-profile-button-by-vexels.png',
                  }} style={styles.profileStyle}
                   ></Image>
               </View>

               <View style={styles.name_status}>
                <Text>{"Name"}</Text>
                <Text style={{marginTop:11}}>{"Status"}</Text>
               </View>
               
               <View style={styles.buttonStyle}>
                   <TouchableOpacity
                   onPress={()=>{
                       this.props.navigation.goBack();
                   }}
                   >
                        <Text style={styles.text}>{"Following"}</Text>
                   </TouchableOpacity>
                   <TouchableOpacity>
                   <Text style={styles.text} 
                   onPress = {()=>{alert("Do you really wanna to unfollow?")}}
                   >{"Unfollow"}</Text>
                   </TouchableOpacity>
               </View>

           </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        marginTop:50,
        alignItems:'center',
        alignContent:'center',
        flex:1
    },
    name_status:{
        justifyContent:'space-between',
        flexDirection:'column',
        marginTop:10
    },
    profileStyle:{
        width:100,
        height:100
    },
    buttonStyle:{
        marginTop: 30,
        justifyContent:'space-evenly',
        width:'100%',
        flexDirection: 'row',
    },
    text: {
        textAlign: 'center',
        // fontWeight: 'bold',
        padding: 5,
        borderRadius:5,
        borderColor: 'black',
        backgroundColor: '#bb81e6',
      },
})





