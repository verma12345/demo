import React, {Component} from 'react';
import {View, StyleSheet, Image, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

alertMessage = (item) =>{
  alert(item.name)
}

export default class Friend_Row extends Component {

  
  render() {
    return (
      <View style={styles.singleuser}>
        <View>
          <Image source={{uri: this.props.item.pic,}}
          style={styles.image_l}/>
        </View>
        <View style={styles.row}>
          <Text style={styles.font}>{this.props.item.name}</Text>
          <Text>{this.props.item.contact}</Text>
        </View>

        <View style={styles.btn_view}>
          {
            <TouchableOpacity>
            <Text style={styles.follow_layout}
            onPress = {() =>{
              this.props.navigation.navigate("Profile") 
            } }
            >{this.props.item.isFollow ? "Following" : "Follow"}</Text>
            </TouchableOpacity>
            
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  singleuser: {
    flex:1,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    width: '100%',
    margin: 5,
  },
  font: {
    fontSize: 18,
  },
  btn_view: {
    alignItems: 'flex-end',
    alignContent: 'flex-end',
    marginHorizontal: 40,
  },
  image_l: {
    height: 50,
    width: 50,
  },
  navigationBar: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: '#9a56cc',
  },
  row: {
    flex:3,
    flexDirection: 'column',
    alignContent: 'space-between',
    marginLeft: 18,
  },
  
  follow_layout: {
    backgroundColor: '#bb81e6',
    width: 80,
    color:'#ffffff',
    padding: 5,
    borderRadius: 5,
    textAlign: 'center',
    alignSelf:'flex-end',
    flexDirection: 'row',
  },
});
