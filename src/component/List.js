import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, } from "react-native";

export default class List extends Component{
    state = ({
        contact:[
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            {name:"jazz",phone:78286275423},
            
        ]
    })

    alertMessage = (item) =>{
        alert(item.name)
    }
    render(){
        return(
            <View>
                {
                    this.state.contact.map((item,index) =>(
                        <TouchableOpacity 
                        key = {item.name}
                        style = {styles.container}
                        onPress = {() => this.alertMessage(item)}
                        >
                            <Text style = {styles.tx}>
                                {item.name}
                            </Text>

                        </TouchableOpacity>
                    ))
                }
            </View>
        )
    }
    
}
const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        backgroundColor:'#afb0ab'
    },
    tx:{
        color:'#000000'
    }
})