import React, {Component} from 'react';
import {View, StyleSheet, FlatList, Text} from 'react-native';
import ComponentView from './ComponentView';

export default class FlatList2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          id: '3',
          name: 'name4',
          isShown: true,
        },
        {
          id: '2',
          name: 'name2',
          isShown: false,
        },
      ],
      title: 'Abcd2',
    };
  }

  _renderItem = item => {
    return <ComponentView item={item.item} />;
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navigationBar}>
          <Text>{this.state.title}</Text>
        </View>
        <FlatList
          data={this.state.data}
          renderItem ={this._renderItem }
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  navigationBar: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: '#1fecf0',
  },
  itemStyle: {
    margin: 18,
    borderRadius: 3,
  },
});
