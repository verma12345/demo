import React, {Component} from 'react';
import {View, StyleSheet, FlatList, Text, StatusBar} from 'react-native';
import ComponentView from './ComponentView';
import Friend_Row from './Friend_Row';

export default class FlatList1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          id: '1',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: true
        },
        {
          id: '2',
          name: 'Kaur',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/980/886/male-portrait-avatar-png-clip-art.png",
          isFollow: false
        },
        {
          id: '3',
          name: 'Akhil',
          contact:7647806487,
          pic: "https://www.pinclipart.com/picdir/middle/78-780477_about-us-avatar-icon-red-png-clipart.png",
          isFollow: false
        },
        {
          id: '4',
          name: 'Divyans',
          contact:6747867438,
          pic: "https://img.pngio.com/parent-directory-avatar-2png-avatar-png-256_256.png",
          isFollow: true
        },
        {
          id: '5',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png",
          isFollow: false
        },
        {
          id: '6',
          name: 'Raj',
          contact:6781643876,
          pic: "https://img.icons8.com/officel/2x/avatar.png",
          isFollow: true
        },
        {
          id: '7',
          name: 'Kamal',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/136/22/profile-icon-illustration-user-profile-computer-icons-girl-customer-avatar-png-clip-art.png",
          isFollow: false
        },
        {
          id: '8',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/348/800/man-wearing-blue-shirt-illustration-png-clip-art.png",
          isFollow: true
        },
        {
          id: '9',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: false
        },
        {
          id: '1',
          name: 'Ranjan',
          contact:6781643876,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: true
        },
        {
          id: '2',
          name: 'Kaur',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/980/886/male-portrait-avatar-png-clip-art.png",
          isFollow: true
        },
        {
          id: '3',
          name: 'Akhil',
          contact:7647806487,
          pic: "https://www.pinclipart.com/picdir/middle/78-780477_about-us-avatar-icon-red-png-clipart.png",
          isFollow: false
        },
        {
          id: '4',
          name: 'Akshita',
          contact:6747867438,
          pic: "https://img.pngio.com/parent-directory-avatar-2png-avatar-png-256_256.png",
          isFollow: true
        },
        {
          id: '5',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png",
          isFollow: true
        },
        {
          id: '6',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://img.icons8.com/officel/2x/avatar.png",
          isFollow: true
        },
        {
          id: '7',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/136/22/profile-icon-illustration-user-profile-computer-icons-girl-customer-avatar-png-clip-art.png",
          isFollow: true
        },
        {
          id: '8',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/348/800/man-wearing-blue-shirt-illustration-png-clip-art.png",
          isFollow: true
        },
        {
          id: '9',
          name: 'Aman preet',
          contact:6487346348,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: true
        },
        {
          id: '1',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: false
        },
        {
          id: '2',
          name: 'Kaur',
          contact:6478643689,
          pic: "https://f0.pngfuel.com/png/980/886/male-portrait-avatar-png-clip-art.png",
          isFollow: true
        },
        {
          id: '3',
          name: 'Akhil',
          contact:7647806487,
          pic: "https://www.pinclipart.com/picdir/middle/78-780477_about-us-avatar-icon-red-png-clipart.png",
          isFollow: true
        },
        {
          id: '4',
          name: 'Akshita',
          contact:6747867438,
          pic: "https://img.pngio.com/parent-directory-avatar-2png-avatar-png-256_256.png",
          isFollow: true
        },
        {
          id: '5',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png",
          isFollow: true
        },
        {
          id: '6',
          name: 'Sumit',
          contact:6781643876,
          pic: "https://img.icons8.com/officel/2x/avatar.png",
          isFollow: true
        },
        {
          id: '7',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/136/22/profile-icon-illustration-user-profile-computer-icons-girl-customer-avatar-png-clip-art.png",
          isFollow: true
        },
        {
          id: '8',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/348/800/man-wearing-blue-shirt-illustration-png-clip-art.png",
          isFollow: true
        },
        {
          id: '9',
          name: 'Garg',
          contact:6781643876,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: true
        },
        {
          id: '1',
          name: 'Dipasu',
          contact:6781643876,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: true
        },
        {
          id: '2',
          name: 'Kaur',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/980/886/male-portrait-avatar-png-clip-art.png",
          isFollow: true
        },
        {
          id: '3',
          name: 'Akhil',
          contact:7647806487,
          pic: "https://www.pinclipart.com/picdir/middle/78-780477_about-us-avatar-icon-red-png-clipart.png",
          isFollow: true
        },
        {
          id: '4',
          name: 'Akshita',
          contact:6747867438,
          pic: "https://img.pngio.com/parent-directory-avatar-2png-avatar-png-256_256.png",
          isFollow: true
        },
        {
          id: '5',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png",
          isFollow: true
        },
        {
          id: '6',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://img.icons8.com/officel/2x/avatar.png",
          isFollow: true
        },
        {
          id: '7',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/136/22/profile-icon-illustration-user-profile-computer-icons-girl-customer-avatar-png-clip-art.png",
          isFollow: true
        },
        {
          id: '8',
          name: 'Amandeep',
          contact:6781643876,
          pic: "httinstall react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-viewps://f0.pngfuel.com/png/348/800/man-wearing-blue-shirt-illustration-png-clip-art.png",
          isFollow: true
        },
        {
          id: '9',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: true
        },
        {
          id: '1',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: true
        },
        {
          id: '2',
          name: 'Kaur',
          contact:6781643876,
          pic: "htinstall react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-viewtps://f0.pngfuel.com/png/980/886/male-portrait-avatar-png-clip-art.png",
          isFollow: true
        },
        {
          id: '3',
          name: 'Akhil',
          contact:7647806487,
          pic: "https://www.pinclipart.com/picdir/middle/78-780477_about-us-avatar-icon-red-png-clipart.png",
          isFollow: true
        },
        {
          id: '4',
          name: 'Akshita',
          contact:6747867438,
          pic: "https://img.pngio.com/parent-directory-avatar-2png-avatar-png-256_256.png",
          isFollow: true
        },
        {
          id: '5',
          name: 'Robin',
          contact:6781643876,
          pic: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png",
          isFollow: true
        },
        {
          id: '6',
          name: 'Steeb',
          contact:6781643876,
          pic: "https://img.icons8.com/officel/2x/avatar.png",
          isFollow: true
        },
        {
          id: '7',
          name: 'Khajama',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/136/22/profile-icon-illustration-user-profile-computer-icons-girl-customer-avatar-png-clip-art.png",
          isFollow: true
        },
        {
          id: '8',
          name: 'Gim',
          contact:6781643876,
          pic: "https://f0.pngfuel.com/png/348/800/man-wearing-blue-shirt-illustration-png-clip-art.png",
          isFollow: true
        },
        {
          id: '9',
          name: 'Amandeep',
          contact:6781643876,
          pic: "https://i.ya-webdesign.com/images/avatar-png-1.png",
          isFollow: true
        },
      ],
      title: 'Instagram',
    };
  }
  // Friend_RowFriend_Row
  _renderItem = data => {
    return <Friend_Row item={data.item}
    navigation= {this.props.navigation} />
  };

 

 

  render() {
    return (
      
      <View style={styles.container}>

        <StatusBar barStyle = "light-content" color = '#ffffff'
        hidden = {false} backgroundColor = "#7103a6" 
        translucent = {true}
        >
        </StatusBar>

        <View style={styles.navigationBar}>
          <Text style={styles.txtcolor}>{this.state.title}</Text>
        </View>
        <FlatList data={this.state.data} renderItem={this._renderItem} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  navigationBar: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginTop:23,
    // marginTop:23,
    backgroundColor: '#9211cf',
  },
  txtcolor:{
    color:'#ffffff'
  },
  itemStyle: {
    margin: 18,
    borderRadius: 3,
    alignSelf:'flex-end'
  },
  
});
