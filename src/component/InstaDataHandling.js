import { Component } from "react";
import InstaMainPage from "./Friend_Row";

export default class InstaDataHandling extends Component{
    constructor(props){
        super(props);
        this.state= {
            profile:[
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                {name:'Jaswant Raj', status:'React Native Devloper'},
                
            ]
        }
    }

    renderItem = item => {
        return <InstaMainPage item={item.item} />;
      };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navigationBar}>
          <Text>{this.state.title}</Text>
        </View>
        <FlatList data={this.state.data} renderItem={this._renderItem} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  navigationBar: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: '#1fecf0',
  },
  itemStyle: {
    margin: 18,
    borderRadius: 3,
  },
});


