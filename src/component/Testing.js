import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {Directions} from 'react-native-gesture-handler';

export default class Register extends Component {
  render() {
    return (
      <ImageBackground
        source={{
          uri:
            'https://www.itl.cat/pngfile/big/212-2125770_best-52-android-material-wallpaper-4k-android.png',
        }}
        style={{flex: 1}}>
        <View style={styles.center}>
          <Text style={styles.myState}>Browser</Text>

          
        </View>

        <View style={styles.alin1} >

            <TouchableOpacity  >
              <Text style={styles.text}>Google</Text>
            </TouchableOpacity>

            <TouchableOpacity  >
              <Text style={styles.text}>Fcebook</Text>
            </TouchableOpacity>
            
            <TouchableOpacity  >
              <Text style={styles.text}>YouTub</Text>
            </TouchableOpacity>

            <TouchableOpacity  >
              <Text style={styles.text}>Gmail</Text>
            </TouchableOpacity>

          </View>

        <View style={styles.alin1} >

            <TouchableOpacity  >
              <Text style={styles.text}></Text>
            </TouchableOpacity>

            <TouchableOpacity  >
              <Text style={styles.text}>Login</Text>
            </TouchableOpacity>
            
            <TouchableOpacity  >
              <Text style={styles.text}>Sign Up</Text>
            </TouchableOpacity>

            <TouchableOpacity  >
              <Text style={styles.text}>Login</Text>
            </TouchableOpacity>

          </View>

      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  center: {
    marginTop:50,
    alignItems: 'center',
  },
  alin1: {
    marginTop: 50,
    justifyContent:'space-evenly',
    width:'100%',
    flexDirection: 'row',
  },
  input: {
   
    width: 350,
    margin: 15,
    // height: 40,

    borderRadius: 33,
    backgroundColor: '#ffffff',
    borderColor: '#7a42f4',
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 115,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },
  myState: {
    marginTop: 20,
    textAlign: 'center',
    color: 'black',
    fontWeight: 'bold',
    fontSize: 25,
  },

  linkState: {
    marginTop: 15,
    color: 'black',
    fontSize: 18,
  },
  text: {
    textAlign: 'center',
    borderWidth: 1,
    fontWeight: 'bold',
    width: 100,
    borderRadius: 80,
    padding: 10,
    borderColor: 'black',
    backgroundColor: 'cyan',
  },
});
