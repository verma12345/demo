import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ImageBackground
} from "react-native";

export default class Login extends Component {
  constructor(props){
    super(props)
    this.state={
      pass:"",
      email:''
    }
  }
  passwordTxt =(item)=>{
    this.setState({
      pass:item
    })
  }
  emailTxt = (item)=>{
    this.setState({
      email:item
    })
  }
  render() {
    return (
      <ImageBackground
        source={{
          uri:"https://i.imgur.com/kxUyuPUh.png"
        }}
        style={{ flex: 1 }}
      >
        <View style={styles.center}>

          <Text style={styles.myState} >Login Account</Text>

          <TextInput
            style={styles.input}
            underlineColorAndroid="transparent"
            placeholder="Email"
            placeholderTextColor="#9a73ef"
            autoCapitalize="none"
            onChangeText={this.emailTxt}
          />

          <TextInput
            style={styles.input}
            underlineColorAndroid="transparent"
            placeholder="Password"
            secureTextEntry = {true}
            placeholderTextColor="#9a73ef"
            autoCapitalize="none"
            onChangeText={this.passwordTxt}
          />

          
        </View>

        <View style={styles.alin1} >

            <TouchableOpacity >
              <Text style={styles.text}>Login</Text>
            </TouchableOpacity> 
          </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  center: {
    marginTop:80,
    alignItems: "center"
  },

  alin1: {
    marginTop: 50,
    justifyContent:'space-evenly',
    width:'100%',
    flexDirection: 'row',
  },
  input: {
    width: 350,
    marginTop:100,
    // height: 40,
    borderRadius: 33,
    backgroundColor: '#93e9f5',
    borderColor: '#7a42f4',
  },
  submitButton: {
    backgroundColor: "#7a42f4",
    padding: 10,
    margin: 115,
    height: 40
  },
  submitButtonText: {
    color: "white"
  },
  myState: {
    marginTop: 20,
    textAlign: "center",
    color: "blue",
    fontWeight: "bold",
    fontSize: 20
  },
  text: {
    textAlign: 'center',
    borderWidth: 1,
    fontWeight: 'bold',
    width: 100,
    borderRadius: 80,
    padding: 10,
    borderColor: 'black',
    backgroundColor: 'cyan',
  },
});