/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import List from './src/component/List'
import FlatList1 from './src/component/FlatList1'
import Register from './src/component/Register'
import Login from './src/component/Login'

import FlatList2 from './src/component/FlatList2'
import InstaMainPage from './src/component/Friend_Row'
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import ProfileScreen from './src/component/ProfileScreen'


const FriendList = createStackNavigator({ 
  Login1: Login,
  Friend: FlatList1,
  Profile:ProfileScreen, }, { headerMode: 'none' });


const AppContainer = createAppContainer(
  createSwitchNavigator({
    FriendL:FriendList, 
  })
)

export default class App extends Component{
  render(){
    return (
      <AppContainer/>
   )
  }
}
